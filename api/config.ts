import dotEnv from 'dotenv';

dotEnv.config();

const config = {
  port: parseInt(process.env.SERVER_PORT),
  database: {
    name: process.env.DATABASE_NAME,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
  },
};

export default config;
