import * as express from 'express';

import ProductsController from '../controllers/products.controller';
import CartController from '../controllers/cart.controller';

const router = express.Router();

const products = new ProductsController();
const cart = new CartController();

router.get('/products', products.getProducts);

router.post('/add-to-cart', cart.addToCart);
router.post('/remove-from-cart', cart.removeFromCart);
router.post('/cart', cart.getUserCartItems);

export default router;
