import mongoose from 'mongoose';
import { ProductsSchema } from './products.model';

export const CartSchema = new mongoose.Schema(
  {
    product_id: { type: String, required: true },
    user_id: { type: String, required: true },
    product: ProductsSchema,
    total: { type: Number, required: true },
  },
  { timestamps: { createdAt: 'createdAt' }, versionKey: false }
);

export default mongoose.model('Cart', CartSchema);
