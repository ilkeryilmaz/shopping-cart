import { Request, Response } from 'express';

import Products from '../models/products.model';

class ProductsController {
  public async getProducts(req: Request, res: Response) {
    try {
      const allProducts = await Products.find();
      return res.json(allProducts);
    } catch (error) {
      return res.status(500).json();
    }
  }
}

export default ProductsController;
