import { Request, Response } from 'express';

import Cart from '../models/cart.model';
import Products from '../models/products.model';
import { DEFAULT_PRODUCT_CART_TOTAL } from '../constants/cart';

class ProductsController {
  public async getUserCartItems(req: Request, res: Response) {
    const { user_id } = req.body;

    if (!user_id) {
      return res.status(500).json();
    }

    try {
      const userCardProducts = await Cart.find({
        user_id: user_id,
      });

      return res.send(userCardProducts);
    } catch (error) {
      return res.status(500).json();
    }
  }

  public async addToCart(req: Request, res: Response) {
    const { product_id, user_id } = req.body;

    if (!product_id && !user_id) {
      return res.status(500).json();
    }

    try {
      const cartItems = await Cart.findOne({
        user_id: user_id,
        product_id: product_id,
      });

      const currentProduct = await Products.findOne({
        _id: product_id,
      });

      if (cartItems) {
        const currentTotal =
          // @ts-ignore
          parseInt(cartItems.total) + DEFAULT_PRODUCT_CART_TOTAL;

        await Cart.updateOne({ _id: cartItems._id }, { total: currentTotal });
      } else {
        await new Cart({
          product_id,
          user_id,
          product: currentProduct,
          total: DEFAULT_PRODUCT_CART_TOTAL,
        }).save();
      }

      const userCardProducts = await Cart.find({
        user_id: user_id,
      });

      return res.send(userCardProducts);
    } catch (error) {
      return res.status(500).json();
    }
  }

  public async removeFromCart(req: Request, res: Response) {
    const { product_id, user_id } = req.body;

    if (!user_id && !product_id) {
      return res.status(500).json();
    }

    try {
      await Cart.deleteOne({
        user_id: user_id,
        product_id: product_id,
      });

      const userCardProducts = await Cart.find({
        user_id: user_id,
      });

      return res.send(userCardProducts);
    } catch (error) {
      return res.status(500).json();
    }
  }
}

export default ProductsController;
