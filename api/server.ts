import bodyParser from 'body-parser';
import cors from 'cors';

import ServerInit from './utils/serverInit';
import config from './config';
import ApiRoutes from './routes/api';

const app = new ServerInit({
  port: config.port,
  routes: {
    api: ApiRoutes,
  },
  middlewares: [
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    cors(),
  ],
  database: {
    name: config.database.name,
    username: config.database.username,
    password: config.database.password,
  },
});

app.listen();
