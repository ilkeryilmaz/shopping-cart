import express, { Application } from 'express';
import * as http from 'http';
import { NextHandleFunction } from 'connect';
import Mongoose from 'mongoose';

interface IRoutes {
  [prefix: string]: express.Router;
}

interface IDatabaseConfig {
  name: string;
  username: string;
  password: string;
}

class ServerInit {
  public app: Application;
  public server: http.Server;
  public port: number;

  constructor(config: {
    port: number;
    middlewares: NextHandleFunction[];
    routes: IRoutes;
    database: IDatabaseConfig;
  }) {
    this.app = express();
    this.port = config.port;

    this.connectDatabase(config.database);
    this.middlewares(config.middlewares);
    this.routes(config.routes);
  }

  public listen(): void {
    this.server = this.app.listen(this.port);
  }

  private connectDatabase(config?: IDatabaseConfig): void {
    const { name, username, password } = config;

    Mongoose.connect(
      `mongodb+srv://${username}:${password}@cluster0.cp4o5.mongodb.net/${name}?retryWrites=true&w=majority`,
      { useUnifiedTopology: true, useNewUrlParser: true }
    );
  }

  private middlewares(middlewares: NextHandleFunction[]): void {
    middlewares.forEach((middleWare) => {
      this.app.use(middleWare);
    });
  }

  private routes(routes: IRoutes): void {
    for (const prefix in routes) {
      const route = routes[prefix];
      this.app.use(`/${prefix}`, route);
    }
  }
}

export default ServerInit;
