# Shopping Cart
A simple shopping cart application. 

## Client
#### Setup
```
cd ./client
```
```
yarn
```
```
yarn start
```

#### Stack:
- React
- Redux, Redux Saga
- Styled-components
- Typescript
- Jest, testing-library
- Storybook

## Api
#### Setup
```
cd ./api
```
```
yarn
```
```
yarn start
```

#### Stack
- NodeJS
- MongoDB
- Typescript
