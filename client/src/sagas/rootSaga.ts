import { SagaIterator } from 'redux-saga';
import { fork, all } from 'redux-saga/effects';

import productsSaga from 'pages/Products/Products.saga';
import cartSaga from 'pages/Cart/Cart.saga';

export default function* rootSaga(): SagaIterator {
  yield all([fork(productsSaga)]);
  yield all([fork(cartSaga)]);
}
