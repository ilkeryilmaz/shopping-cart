import { combineReducers } from 'redux';

import productsReducer from 'pages/Products/Products.reducer';
import cartReducer from 'pages/Cart/Cart.reducer';

const appStateReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
});

export default appStateReducer;
