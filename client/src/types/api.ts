/* eslint-disable  @typescript-eslint/no-explicit-any */

import { AxiosRequestConfig } from 'axios';

export type TRequestParams = {
  params: Record<string, unknown> | undefined;
  config: AxiosRequestConfig | undefined;
} & any;

export type TResponseInterceptor = {
  ok: boolean;
  data: Record<string, unknown>;
  headers: Record<string, unknown>;
} & any;
