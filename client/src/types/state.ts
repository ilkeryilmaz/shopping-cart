import { TProductsState } from 'pages/Products/Products.types';
import { TCartState } from 'pages/Cart/Cart.types';

export type TAppStateProps = {
  products: TProductsState;
  cart: TCartState;
};
