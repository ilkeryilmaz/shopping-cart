import { AxiosPromise } from 'axios';

import { BASE_API_INSTANCE } from 'configs/axiosInstances';
import { TRequestParams } from 'types/api';
import { API_URLS } from 'constants/urls';

export const API = {
  GET_PRODUCTS: ({ params }: TRequestParams): AxiosPromise =>
    BASE_API_INSTANCE.get(API_URLS.PRODUCTS, params),
  ADD_TO_CART: ({ params, config }: TRequestParams): AxiosPromise =>
    BASE_API_INSTANCE.post(API_URLS.ADD_TO_CART, params, config),
  GET_CART: ({ params, config }: TRequestParams): AxiosPromise =>
    BASE_API_INSTANCE.post(API_URLS.CART, params, config),
  REMOVE_FROM_CART: ({ params, config }: TRequestParams): AxiosPromise =>
    BASE_API_INSTANCE.post(API_URLS.REMOVE_FROM_CART, params, config),
};
