import { TColorPrimitives } from '../types';

import colors from '../shared/colors';

const colorPrimitives: TColorPrimitives = {
  // Button
  buttonPrimaryBackground: colors.primary900,
  buttonPrimaryBorder: colors.primary900,
  buttonPrimaryText: '#fff',
  buttonPrimaryHoverBackground: '#fff',
  buttonPrimaryHoverText: colors.primary900,
};

export default colorPrimitives;
