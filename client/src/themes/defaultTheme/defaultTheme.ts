import { TTheme } from '../types';

import breakpoints from '../shared/breakpoints';
import mediaQuery from '../shared/mediaQuery';
import sizing from '../shared/sizing';

import colorPrimitives from './colorPrimitives';

const defaultTheme: TTheme = {
  name: 'defaultTheme',
  colors: {
    ...colorPrimitives,
  },
  breakpoints,
  mediaQuery,
  sizing,
};

export default defaultTheme;
