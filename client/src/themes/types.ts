import { ReactNode } from 'react';

export type TColors = {
  primary900: string;
  secondary900: string;
};

export type TColorPrimitives = {
  buttonPrimaryBackground: string;
  buttonPrimaryBorder: string;
  buttonPrimaryText: string;
  buttonPrimaryHoverBackground: string;
  buttonPrimaryHoverText: string;
};

export type TBreakpoints = {
  small: number;
  medium: number;
  large: number;
};

export type TMediaQuery = {
  small: string;
  medium: string;
  large: string;
};

export type TSizing = {
  scale100: string;
  scale200: string;
  scale300: string;
  scale400: string;
  scale500: string;
  scale600: string;
  scale700: string;
  scale800: string;
};

export type TTheme = {
  name: string;
  colors: Record<string, string>;
  breakpoints: TBreakpoints;
  mediaQuery: TMediaQuery;
  sizing: TSizing;
};

export type TThemeContextProviderProps = {
  children: ReactNode;
};
