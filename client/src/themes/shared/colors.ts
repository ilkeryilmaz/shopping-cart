import { TColors } from '../types';

const colors: TColors = {
  primary900: '#303030',

  secondary900: '#e9e7e7',
};

export default colors;
