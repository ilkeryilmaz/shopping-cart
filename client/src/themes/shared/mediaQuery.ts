import { getMediaQuery } from 'utils/theme';

import { TMediaQuery } from '../types';
import breakpoints from './breakpoints';

const mediaQuery: TMediaQuery = {
  small: getMediaQuery(breakpoints.small),
  medium: getMediaQuery(breakpoints.medium),
  large: getMediaQuery(breakpoints.large),
};

export default mediaQuery;
