import { TBreakpoints } from '../types';

const breakpoints: TBreakpoints = {
  small: 320,
  medium: 600,
  large: 1136,
};

export default breakpoints;
