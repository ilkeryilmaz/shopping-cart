import React, { ReactElement } from 'react';
import { ThemeProvider as StyledComponentThemeProvider } from 'styled-components';

import { TThemeContextProviderProps } from './types';
import defaultTheme from './defaultTheme/defaultTheme';
import GlobalStyle from './styles/GlobalStyle';

function ThemeProvider({ children }: TThemeContextProviderProps): ReactElement {
  return (
    <StyledComponentThemeProvider theme={defaultTheme}>
      <GlobalStyle />
      {children}
    </StyledComponentThemeProvider>
  );
}

export { ThemeProvider };
