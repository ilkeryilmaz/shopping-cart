import axios, { AxiosResponse } from 'axios';

import { TResponseInterceptor } from 'types/api';

export const BASE_API_INSTANCE = axios.create({
  baseURL: process.env.REACT_APP_BASE_API_URL,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
  validateStatus: (status: number): boolean => status < 500,
});

BASE_API_INSTANCE.interceptors.response.use(handleResponseInterceptor);

function handleResponseInterceptor(
  response: AxiosResponse
): TResponseInterceptor {
  return {
    success: response.status >= 200 && response.status < 500,
    data: response.data,
  };
}
