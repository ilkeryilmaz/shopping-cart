import { lazy } from 'react';

const Products = lazy(() => import('pages/Products'));
const Cart = lazy(() => import('pages/Cart'));

export const routes = [
  {
    key: 'products',
    path: '/',
    exact: true,
    component: Products,
  },
  {
    key: 'cart',
    path: '/cart',
    exact: true,
    component: Cart,
  },
];
