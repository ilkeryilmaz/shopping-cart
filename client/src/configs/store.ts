import createSagaMiddleware from 'redux-saga';
import {
  applyMiddleware,
  createStore,
  compose,
  Store,
  CombinedState,
  AnyAction,
} from 'redux';

import rootReducer from 'reducers/rootReducer';
import rootSaga from 'sagas/rootSaga';
import { TAppStateProps } from 'types/state';

const sagaMiddleware = createSagaMiddleware();

function configureStore(): {
  store: Store<CombinedState<TAppStateProps>, AnyAction> &
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Store<any & Record<string, string>, any>;
} {
  const composeEnhancers =
    (process.env.NODE_ENV === 'development' &&
      window &&
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
    compose;

  const store = createStore(
    rootReducer,
    {},
    composeEnhancers(applyMiddleware(sagaMiddleware))
  );

  sagaMiddleware.run(rootSaga);

  return {
    store,
  };
}

export default configureStore;
