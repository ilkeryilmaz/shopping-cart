export const API_URLS = {
  PRODUCTS: '/api/products',
  ADD_TO_CART: '/api/add-to-cart',
  CART: '/api/cart',
  REMOVE_FROM_CART: '/api/remove-from-cart',
};
