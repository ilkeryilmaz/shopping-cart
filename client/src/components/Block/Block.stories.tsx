import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'context/AppContext';

import Block from './Block';
import { TBlockProps } from './Block.types';

export default {
  title: 'Components/Block',
  component: Block,
} as Meta;

const Template: Story<TBlockProps> = (args) => (
  <AppProvider>
    <Block {...args} />
  </AppProvider>
);

export const Primary = Template.bind({});
Primary.args = {
  children: 'Sample Text',
  color: 'red',
  marginTop: '10px',
  marginLeft: '10px',
};
