import { ElementType, ReactNode } from 'react';
import { TTheme } from '../../themes/types';

type TResponsive<T> = T | Array<T>;

type TAlignItems =
  | 'center'
  | 'start'
  | 'end'
  | 'flex-start'
  | 'flex-end'
  | 'normal'
  | 'baseline'
  | 'first baseline'
  | 'last baseline'
  | 'space-between'
  | 'space-around'
  | 'space-evenly'
  | 'stretch'
  | 'safe center'
  | 'unsafe center'
  | 'inherit'
  | 'initial'
  | 'unset';

type TAlignSelf =
  | 'auto'
  | 'normal'
  | 'center'
  | 'start'
  | 'end'
  | 'self-start'
  | 'self-end'
  | 'flex-start'
  | 'flex-end'
  | 'baseline'
  | 'first baseline'
  | 'last baseline'
  | 'stretch'
  | 'safe center'
  | 'unsafe center'
  | 'inherit'
  | 'initial'
  | 'unset';

type TPosition = 'static' | 'absolute' | 'relative' | 'fixed' | 'sticky';

type TFlexDirection =
  | 'row'
  | 'row-reverse'
  | 'column'
  | 'column-reverse'
  | 'inherit'
  | 'initial'
  | 'unset';

type TDisplay =
  | 'block'
  | 'inline'
  | 'run-in'
  | 'flow'
  | 'flow-root'
  | 'table'
  | 'flex'
  | 'grid'
  | 'ruby'
  | 'block flow'
  | 'inline table'
  | 'flex run-in'
  | 'list-item'
  | 'list-item block'
  | 'list-item inline'
  | 'list-item flow'
  | 'list-item flow-root'
  | 'list-item block flow'
  | 'list-item block flow-root'
  | 'flow list-item block'
  | 'table-row-group'
  | 'table-header-group'
  | 'table-footer-group'
  | 'table-row'
  | 'table-cell'
  | 'table-column-group'
  | 'table-column'
  | 'table-caption'
  | 'ruby-base'
  | 'ruby-text'
  | 'ruby-base-container'
  | 'ruby-text-container'
  | 'contents'
  | 'none'
  | 'inline-block'
  | 'inline-table'
  | 'inline-flex'
  | 'inline-grid'
  | 'inherit'
  | 'initial'
  | 'unset';

type TFlex = number | string;

type TJustifyContent =
  | 'center'
  | 'start'
  | 'end'
  | 'flex-start'
  | 'flex-end'
  | 'left'
  | 'right'
  | 'space-between'
  | 'space-around'
  | 'space-evenly'
  | 'stretch'
  | 'safe center'
  | 'unsafe center'
  | 'inherit'
  | 'initial'
  | 'unset';

type TFlexWrap = 'wrap' | 'no-wrap';

type TScale = 0 | string;

export type TTextAlign = 'normal' | 'left' | 'center' | 'right';

type TSharedBlockProps = {
  as?: ElementType;
  color?: TResponsive<string>;
  alignItems?: TResponsive<TAlignItems>;
  alignSelf?: TResponsive<TAlignSelf>;
  flexDirection?: TResponsive<TFlexDirection>;
  display?: TResponsive<TDisplay>;
  flex?: TResponsive<TFlex>;
  flexWrap?: TResponsive<TFlexWrap>;
  justifyContent?: TResponsive<TJustifyContent>;
  width?: TResponsive<TScale>;
  minWidth?: TResponsive<TScale>;
  maxWidth?: TResponsive<TScale>;
  height?: TResponsive<TScale>;
  minHeight?: TResponsive<TScale>;
  maxHeight?: TResponsive<TScale>;
  margin?: TResponsive<TScale>;
  marginTop?: TResponsive<TScale>;
  marginRight?: TResponsive<TScale>;
  marginBottom?: TResponsive<TScale>;
  marginLeft?: TResponsive<TScale>;
  padding?: TResponsive<TScale>;
  paddingTop?: TResponsive<TScale>;
  paddingRight?: TResponsive<TScale>;
  paddingBottom?: TResponsive<TScale>;
  paddingLeft?: TResponsive<TScale>;
  position?: TResponsive<TPosition>;
  left?: TResponsive<TScale>;
  top?: TResponsive<TScale>;
  right?: TResponsive<TScale>;
  bottom?: TResponsive<TScale>;
  textAlign?: TResponsive<TTextAlign>;
};

export type TBlockProps = TSharedBlockProps & {
  children?: ReactNode;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [rest: string]: any;
};

export type TStyledBlockProps = TSharedBlockProps;

export type TAddDeclarationProps = {
  property: string;
  value: string | Array<string>;
  transform?: (arg: string) => string;
  theme: TTheme;
};
