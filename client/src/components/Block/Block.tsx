import React, { ReactElement } from 'react';

import { StyledBlock } from './Block.style';
import { TBlockProps } from './Block.types';

function Block({
  children,
  as = 'div',
  color,
  alignItems,
  alignSelf,
  flexDirection,
  display,
  flex,
  flexWrap,
  justifyContent,
  width,
  minWidth,
  maxWidth,
  height,
  minHeight,
  maxHeight,
  margin,
  marginTop,
  marginRight,
  marginBottom,
  marginLeft,
  padding,
  paddingTop,
  paddingRight,
  paddingBottom,
  paddingLeft,
  position,
  left,
  top,
  right,
  bottom,
  textAlign,
  ...rest
}: TBlockProps): ReactElement {
  return (
    <StyledBlock
      data-testid="Block"
      as={as}
      color={color}
      alignItems={alignItems}
      alignSelf={alignSelf}
      flexDirection={flexDirection}
      display={display}
      flex={flex}
      flexWrap={flexWrap}
      justifyContent={justifyContent}
      width={width}
      minWidth={minWidth}
      maxWidth={maxWidth}
      height={height}
      minHeight={minHeight}
      maxHeight={maxHeight}
      margin={margin}
      marginTop={marginTop}
      marginRight={marginRight}
      marginBottom={marginBottom}
      marginLeft={marginLeft}
      padding={padding}
      paddingTop={paddingTop}
      paddingRight={paddingRight}
      paddingBottom={paddingBottom}
      paddingLeft={paddingLeft}
      position={position}
      left={left}
      top={top}
      right={right}
      bottom={bottom}
      textAlign={textAlign}
      {...rest}>
      {children}
    </StyledBlock>
  );
}

export default Block;
