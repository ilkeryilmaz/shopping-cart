import React from 'react';
import { render } from '@testing-library/react';

import AppProvider from 'context/AppContext';
import breakpoints from 'themes/shared/breakpoints';

import Block from './Block';

describe('Block', () => {
  it('renders the Block with default', () => {
    const { getByTestId } = render(
      <AppProvider>
        <Block display="flex" />
      </AppProvider>
    );

    expect(getByTestId('Block')).toBeDefined();
    expect(getByTestId('Block')).toHaveStyleRule('display', 'flex');
  });

  it('renders the Block with responsive', () => {
    const { getByTestId } = render(
      <AppProvider>
        <Block marginBottom={['10px', '20px', '40px', '80px']} />
      </AppProvider>
    );

    const block = getByTestId('Block');

    expect(block).toHaveStyleRule('margin-bottom', '10px');
    expect(block).toHaveStyleRule('margin-bottom', '20px', {
      media: `screen and (min-width: ${breakpoints.small}px)`,
    });
    expect(block).toHaveStyleRule('margin-bottom', '40px', {
      media: `screen and (min-width: ${breakpoints.medium}px)`,
    });
    expect(block).toHaveStyleRule('margin-bottom', '80px', {
      media: `screen and (min-width: ${breakpoints.large}px)`,
    });
  });
});
