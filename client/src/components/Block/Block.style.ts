import styled from 'styled-components';

import createStyles from 'utils/createStyles';

import { TStyledBlockProps } from './Block.types';


export const StyledBlock = styled.div<TStyledBlockProps>((props) => {
  const {
    theme: { breakpoints },
  } = props;

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const getPropertyValue = (key: string) => props[key];

  const styles = createStyles(breakpoints);

  const declarationProperties = [
    'alignItems',
    'alignSelf',
    'display',
    'flex',
    'flexDirection',
    'flexWrap',
    'justifyContent',
    'position',
    'textAlign',
  ];

  const scaledDeclarationProperties = [
    'width',
    'minWidth',
    'maxWidth',
    'height',
    'minHeight',
    'maxHeight',
    'height',
    'margin',
    'marginTop',
    'marginRight',
    'marginBottom',
    'marginLeft',
    'padding',
    'paddingTop',
    'paddingRight',
    'paddingBottom',
    'paddingLeft',
    'top',
    'right',
    'left',
    'bottom',
  ];

  styles.addDeclaration({
    property: 'color',
    value: getPropertyValue('color'),
    transform: (color: string) => color,
  });

  declarationProperties.forEach((property) => {
    styles.addDeclaration({ property, value: getPropertyValue(property) });
  });

  scaledDeclarationProperties.forEach((property) => {
    styles.addDeclaration({
      property,
      value: getPropertyValue(property),
      transform: (size: string) => size,
    });
  });

  return styles.getStyles();
});
