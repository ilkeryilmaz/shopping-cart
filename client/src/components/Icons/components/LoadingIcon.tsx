import React, { ReactElement } from 'react';

import { TIconProps } from '../Icons.types';

function LoadingIcon({ iconColor, ...rest }: TIconProps): ReactElement {
  return (
    <svg viewBox="0 0 100 100" width={100} height={100} {...rest}>
      <circle fill={iconColor || '#303030'} cx={6} cy={50} r={6}>
        <animateTransform
          attributeName="transform"
          dur="1s"
          type="translate"
          values="0 15 ; 0 -15; 0 15"
          repeatCount="indefinite"
          begin={0.1}
        />
      </circle>
      <circle fill={iconColor || '#303030'} cx={30} cy={50} r={6}>
        <animateTransform
          attributeName="transform"
          dur="1s"
          type="translate"
          values="0 10 ; 0 -10; 0 10"
          repeatCount="indefinite"
          begin={0.2}
        />
      </circle>
      <circle fill={iconColor || '#303030'} cx={54} cy={50} r={6}>
        <animateTransform
          attributeName="transform"
          dur="1s"
          type="translate"
          values="0 5 ; 0 -5; 0 5"
          repeatCount="indefinite"
          begin={0.3}
        />
      </circle>
    </svg>
  );
}

export default LoadingIcon;
