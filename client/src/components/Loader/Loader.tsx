import React, { ReactElement } from 'react';

import { LoadingIcon } from 'components/Icons';

import { BaseLoader } from './Loader.style';

function Loader(): ReactElement {
  return (
    <BaseLoader data-testid="Loader">
      <LoadingIcon data-testid="LoaderIcon" />
    </BaseLoader>
  );
}

export default Loader;
