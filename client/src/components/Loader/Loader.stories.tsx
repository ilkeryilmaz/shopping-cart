import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'context/AppContext';

import Loader from './Loader';

export default {
  title: 'Components/Loader',
  component: Loader,
} as Meta;

const Template: Story = (args) => (
  <AppProvider>
    <Loader {...args} />
  </AppProvider>
);

export const Primary = Template.bind({});
