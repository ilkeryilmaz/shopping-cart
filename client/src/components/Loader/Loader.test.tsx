import React from 'react';
import { render } from '@testing-library/react';

import Loader from './Loader';

describe('Loader', () => {
  it('renders the Loader with default', () => {
    const { getByTestId } = render(<Loader />);

    expect(getByTestId('Loader')).toBeDefined();
    expect(getByTestId('LoaderIcon')).toBeDefined();
  });
});
