export const TITLE_VARIANT = {
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
} as const;

export const COLOR = {
  inherit: 'inherit',
  primary: 'primary',
  secondary: 'secondary',
} as const;

export const TEXT_VARIANT = {
  p: 'p',
  span: 'span',
  label: 'label',
  div: 'div',
} as const;

export const TEXT_SIZE = {
  small: 'small',
  medium: 'medium',
  large: 'large',
} as const;
