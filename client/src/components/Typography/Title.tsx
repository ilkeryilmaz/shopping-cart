import React, { ReactElement } from 'react';

import { TTitleProps } from './Typography.types';
import { BaseTitle } from './Typography.style';
import { TITLE_VARIANT, COLOR } from './Typography.constants';

function Title({
  variant = TITLE_VARIANT.h1,
  color = COLOR.inherit,
  children,
  ...restProps
}: TTitleProps): ReactElement {
  const component = TITLE_VARIANT[variant];

  return (
    <BaseTitle as={component} variant={component} color={color} {...restProps}>
      {children}
    </BaseTitle>
  );
}

export default Title;
