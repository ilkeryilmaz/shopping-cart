import styled from 'styled-components';

import { TEXT_SIZE, TITLE_VARIANT, COLOR } from './Typography.constants';
import {
  TStyledBaseTextProps,
  TStyledBaseTitleProps,
  TTitleVariantStyleProps,
  TTextSizeStylesProps,
  TColorStylesProps,
  TFontStylesProps,
  TFontStyles,
} from './Typography.types';

export const BaseText = styled.span<TStyledBaseTextProps>(
  ({ color, size, bold, medium, underline }) => ({
    ...getTextSizeStyles({ size }),
    ...getFontStyles({ underline, medium, bold }),
    ...getColorStyles({ color }),
  })
);

export const BaseTitle = styled.div<TStyledBaseTitleProps>(
  ({ color, variant }) => ({
    ...getTitleVariantStyles({ variant }),
    ...getColorStyles({ color }),
  })
);

function getTitleVariantStyles({
  variant,
}: TTitleVariantStyleProps): Record<string, number> {
  switch (variant) {
    case TITLE_VARIANT.h1:
      return { fontSize: 32 };
    case TITLE_VARIANT.h2:
      return { fontSize: 28 };
    case TITLE_VARIANT.h3:
      return { fontSize: 24 };
    case TITLE_VARIANT.h4:
      return { fontSize: 20 };
    default:
      return { fontSize: 20 };
  }
}

function getTextSizeStyles({
  size,
}: TTextSizeStylesProps): Record<string, number> {
  switch (size) {
    case TEXT_SIZE.small:
      return { fontSize: 12 };
    case TEXT_SIZE.medium:
      return { fontSize: 16 };
    default:
    case TEXT_SIZE.large:
      return { fontSize: 18 };
  }
}

function getColorStyles({ color }: TColorStylesProps): Record<string, string> {
  switch (color) {
    case COLOR.primary:
      return { color: '#7e2ede' };
    case COLOR.secondary:
      return { color: '#909090' };
    default:
      return { color: 'inherit' };
  }
}

function getFontStyles({
  underline,
  medium,
  bold,
}: TFontStylesProps): TFontStyles {
  if (underline) {
    return {
      textDecoration: 'underline',
    };
  }

  if (medium) {
    return {
      fontWeight: 500,
    };
  }

  if (bold) {
    return {
      fontWeight: 600,
    };
  }

  return {};
}
