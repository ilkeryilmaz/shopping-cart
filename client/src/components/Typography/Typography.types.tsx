import { ReactNode, HTMLAttributes } from 'react';

import {
  COLOR,
  TEXT_VARIANT,
  TEXT_SIZE,
  TITLE_VARIANT,
} from './Typography.constants';

export type TColor = keyof typeof COLOR;
export type TTextVariant = keyof typeof TEXT_VARIANT;
export type TTextSize = keyof typeof TEXT_SIZE;
export type TTitleVariant = keyof typeof TITLE_VARIANT;

export type TStyledBaseTextProps = HTMLAttributes<HTMLDivElement> & {
  size?: TTextSize;
  color?: TColor;
  bold?: boolean;
  medium?: boolean;
  underline?: boolean;
};

export type TStyledBaseTitleProps = HTMLAttributes<HTMLDivElement> & {
  variant?: TTitleVariant;
};

export type TTitleVariantStyleProps = {
  variant?: TTitleVariant;
};

export type TTextProps = {
  color?: TColor;
  size?: TTextSize;
  variant?: TTextVariant;
  bold?: boolean;
  medium?: boolean;
  underline?: boolean;
  children?: ReactNode;
};

export type TTitleProps = {
  variant?: TTitleVariant;
  color?: TColor;
  textId?: string;
  children?: ReactNode;
};

export type TTextSizeStylesProps = {
  size?: TTextSize;
};

export type TTextSizeStyles = {
  color?: string;
};

export type TColorStylesProps = {
  color?: string;
};

export type TFontStylesProps = {
  underline?: boolean;
  medium?: boolean;
  bold?: boolean;
};

export type TFontStyles = {
  textDecoration?: string;
  fontWeight?: 'bold' | 'normal' | 'bolder' | 'lighter' | number;
};
