import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import Text from './Text';
import Title from './Title';
import { TTextProps, TTitleProps } from './Typography.types';

export default {
  title: 'components/Typography',
  component: Text,
} as Meta;

const TextTemplate: Story<TTextProps> = (args) => <Text {...args} />;

const TitleTemplate: Story<TTitleProps> = (args) => <Title {...args} />;

export const TextDefault = TextTemplate.bind({});
TextDefault.args = {
  children: 'Sample Text',
};

export const TitleDefault = TitleTemplate.bind({});
TitleDefault.args = {
  children: 'Sample Text',
};
