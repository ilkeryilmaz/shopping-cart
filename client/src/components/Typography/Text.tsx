import React, { ReactElement } from 'react';

import { TTextProps } from './Typography.types';
import { BaseText } from './Typography.style';
import { COLOR, TEXT_SIZE, TEXT_VARIANT } from './Typography.constants';

function Text({
  color = COLOR.inherit,
  size = TEXT_SIZE.medium,
  variant = TEXT_VARIANT.span,
  bold,
  medium,
  underline,
  children,
  ...restProps
}: TTextProps): ReactElement {
  return (
    <BaseText
      as={TEXT_VARIANT[variant]}
      size={size}
      color={color}
      bold={bold}
      medium={medium}
      underline={underline}
      {...restProps}
    >
      {children}
    </BaseText>
  );
}

export default Text;
