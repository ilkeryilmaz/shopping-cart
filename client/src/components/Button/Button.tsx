import React, { ReactElement } from 'react';

import { BaseButton } from './Button.style';
import { TButtonProps } from './Button.types';
import { BUTTON_VARIANT } from './Button.constants';

function Button({
  variant = BUTTON_VARIANT.primary,
  onClick,
  children,
}: TButtonProps): ReactElement {
  return (
    <BaseButton variant={variant} onClick={onClick} data-testid="Button">
      {children}
    </BaseButton>
  );
}

export default Button;
