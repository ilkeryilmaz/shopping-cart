import { ButtonHTMLAttributes, ReactNode } from 'react';

import { TTheme } from 'themes/types';
import { BUTTON_VARIANT } from './Button.constants';

export type TButtonVariant = keyof typeof BUTTON_VARIANT;

export type TButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  variant?: TButtonVariant;
  children: ReactNode;
};

export type TSharedButtonStylesProps = {
  variant?: TButtonVariant;
  theme: TTheme;
};

export type TButtonVariantStyles = {
  color: string;
  background: string;
  borderColor: string;
  ':hover': {
    background: string;
    color: string;
  };
};
