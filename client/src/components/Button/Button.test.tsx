import React from 'react';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Button from './Button';

describe('Button', () => {
  const mockText = 'Button';

  it('renders the Button with default', () => {
    const { getByTestId, getByText } = render(<Button>{mockText}</Button>);

    expect(getByTestId('Button')).toBeDefined();
    expect(getByText(mockText)).toBeDefined();
  });

  it('should call onClick when clicked', () => {
    const onClickMock = jest.fn();

    const { getByTestId } = render(
      <Button onClick={onClickMock}>{mockText}</Button>
    );

    expect(onClickMock).toBeCalledTimes(0);
    userEvent.click(getByTestId('Button'));
    expect(onClickMock).toBeCalledTimes(1);
  });
});
