import styled from 'styled-components';

import { TTheme } from 'themes/types';
import { TSharedButtonStylesProps, TButtonVariantStyles } from './Button.types';

export const BaseButton = styled.button<TSharedButtonStylesProps>(
  ({ theme }) => ({
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderStyle: 'solid',
    padding: '8px 16px',
    cursor: 'pointer',
    fontSize: 14,
    transition: 'all 0.2s linear',
    ...getButtonVariantStyles(theme),
  })
);

function getButtonVariantStyles(theme: TTheme): TButtonVariantStyles {
  return {
    color: theme.colors.buttonPrimaryText,
    background: theme.colors.buttonPrimaryBackground,
    borderColor: theme.colors.buttonPrimaryBorder,
    ':hover': {
      background: theme.colors.buttonPrimaryHoverBackground,
      color: theme.colors.buttonPrimaryHoverText,
    },
  };
}
