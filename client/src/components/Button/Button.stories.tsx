import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'context/AppContext';

import Button from './Button';
import { TButtonProps } from './Button.types';

export default {
  title: 'Components/Button',
  component: Button,
} as Meta;

const Template: Story<TButtonProps> = (args) => (
  <AppProvider>
    <Button {...args} />
  </AppProvider>
);

export const Primary = Template.bind({});
Primary.args = {
  children: 'Sample Text',
};
