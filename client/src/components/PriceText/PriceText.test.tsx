import React from 'react';
import { render } from '@testing-library/react';

import PriceText from './PriceText';
import { CURRENCY_MAP } from './PriceText.constants';

describe('PriceText', () => {
  it('renders the PriceText with default', () => {
    const { getByTestId } = render(<PriceText />);

    expect(getByTestId('PriceTextCurrency')).toHaveTextContent(
      CURRENCY_MAP.USD
    );
    expect(getByTestId('PriceTextPrice')).toHaveTextContent('0.00');
  });

  it('should return correct price', () => {
    const { getByTestId, rerender } = render(<PriceText price={1234.56} />);

    expect(getByTestId('PriceTextPrice')).toHaveTextContent('1,234.56');
    rerender(<PriceText price={134} />);
    expect(getByTestId('PriceTextPrice')).toHaveTextContent('134.00');
  });
});
