export const CURRENCY_MAP = {
  USD: '$',
  EUR: '€',
} as const;

export const DEFAULT_FRACTION = 2;
