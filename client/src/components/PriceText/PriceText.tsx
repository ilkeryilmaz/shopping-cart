import React, { ReactElement } from 'react';

import { Text } from 'components/Typography';

import { LOCALE } from 'constants/locale';

import { TPriceText } from './PriceText.types';
import { CURRENCY_MAP, DEFAULT_FRACTION } from './PriceText.constants';

function PriceText({
  price = 0,
  currency = CURRENCY_MAP.USD,
}: TPriceText): ReactElement {
  const formattedPrice = Intl.NumberFormat(LOCALE.EN, {
    style: 'decimal',
    useGrouping: true,
    minimumFractionDigits: DEFAULT_FRACTION,
  }).format(price);

  return (
    <>
      <Text size="medium" data-testid="PriceTextCurrency">
        {currency}
      </Text>
      <Text size="medium" data-testid="PriceTextPrice">
        {formattedPrice}
      </Text>
    </>
  );
}

export default PriceText;
