import { CURRENCY_MAP } from './PriceText.constants';

export type TCurrency = typeof CURRENCY_MAP[keyof typeof CURRENCY_MAP];

export type TPriceText = {
  currency?: TCurrency;
  price?: number;
};
