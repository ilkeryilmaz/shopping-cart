import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'context/AppContext';

import PriceText from './PriceText';
import { TPriceText } from './PriceText.types';

export default {
  title: 'Components/PriceText',
  component: PriceText,
} as Meta;

const Template: Story<TPriceText> = (args) => (
  <AppProvider>
    <PriceText {...args} />
  </AppProvider>
);

export const Primary = Template.bind({});
Primary.args = {
  price: 10,
};
