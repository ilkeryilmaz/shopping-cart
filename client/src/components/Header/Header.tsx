import React, { ReactElement, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { Title, Text } from 'components/Typography';
import { TCartState } from 'pages/Cart/Cart.types';
import { TAppStateProps } from 'types/state';

import { Root, NavLinks } from './Header.style';
import { THeaderProps } from './Header.types';
import * as cartActions from '../../pages/Cart/Cart.actions';

function Header({ storeName }: THeaderProps): ReactElement {
  const dispatch = useDispatch();
  const { cartItems } = useSelector(
    (state: TAppStateProps): TCartState => state.cart
  );

  useEffect(() => {
    dispatch(cartActions.cartRequestGetCartItems());
  }, []);

  return (
    <Root>
      <Title>{storeName}</Title>
      <NavLinks>
        <Link to="/">
          <Text size="large">Products</Text>
        </Link>
        <Link to="cart">
          <Text size="large">My Cart ({cartItems.length})</Text>
        </Link>
      </NavLinks>
    </Root>
  );
}

export default Header;
