import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'context/AppContext';

import Header from './Header';
import { THeaderProps } from './Header.types';

export default {
  title: 'Components/Header',
  component: Header,
} as Meta;

const Template: Story<THeaderProps> = (args) => (
  <AppProvider>
    <Header {...args} />
  </AppProvider>
);

export const Primary = Template.bind({});
Primary.args = {
  storeName: 'Store Name',
};
