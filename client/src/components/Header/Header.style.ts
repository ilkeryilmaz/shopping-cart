import styled from 'styled-components';

export const Root = styled.header(({ theme }) => ({
  width: '100%',
  height: 100,
  marginTop: theme.sizing.scale400,
  marginBottom: theme.sizing.scale400,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
}));

export const NavLinks = styled.div(({ theme }) => ({
  '> a': {
    marginLeft: theme.sizing.scale200,
  },
}));
