import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'context/AppContext';

import ProductCard from './ProductCard';
import { TProductCard } from './ProductCard.types';

export default {
  title: 'Components/ProductCard',
  component: ProductCard,
} as Meta;

const DEFAULT_ARGS = {
  name: 'Basic Swear Shirt',
  price: 10,
  imageUrl:
    'https://cdn.shopify.com/s/files/1/0049/2856/9434/products/product-05_1024x.jpg?v=1553074064',
};

const Template: Story<TProductCard> = (args) => (
  <AppProvider>
    <div style={{ width: 350 }}>
      <ProductCard {...args} />
    </div>
  </AppProvider>
);

export const Default = Template.bind({});
Default.args = DEFAULT_ARGS;
