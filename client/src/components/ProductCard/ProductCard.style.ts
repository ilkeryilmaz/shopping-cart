import styled from 'styled-components';

export const Root = styled.div(({ theme }) => ({
  width: '100%',
  borderWidth: 1,
  borderStyle: 'solid',
  borderColor: '#e9e7e7',
  marginLeft: theme.sizing.scale200,
  marginBottom: theme.sizing.scale200,
  borderRadius: 4,
  flex: '1 0 calc(50% - 16px)',
  [theme.mediaQuery.medium]: {
    flex: '1 0 calc(33% - 16px)',
  },
  [theme.mediaQuery.large]: {
    flex: '1 0 calc(25% - 16px)',
  },
}));

export const ImageWrapper = styled.figure(() => ({
  position: 'relative' as const,
  margin: 0,
  padding: 0,
  '> img': {
    width: '100%',
    height: 'auto',
  },
}));

export const InfoWrapper = styled.div(({ theme }) => ({
  padding: theme.sizing.scale200,
}));

export const TitleWrapper = styled.div(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  marginBottom: theme.sizing.scale100,
  height: 50,
}));

export const ActionWrapper = styled.div(({ theme }) => ({
  marginTop: theme.sizing.scale200,
}));
