export type TProductCard = {
  imageUrl: string;
  name: string;
  price: number;
  onClick?: () => void;
};
