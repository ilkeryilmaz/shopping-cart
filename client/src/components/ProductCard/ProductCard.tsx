import React, { ReactElement } from 'react';

import { Title } from 'components/Typography';
import Button from 'components/Button';
import PriceText from 'components/PriceText';

import { TProductCard } from './ProductCard.types';
import {
  Root,
  ImageWrapper,
  InfoWrapper,
  ActionWrapper,
  TitleWrapper,
} from './ProductCard.style';

function ProductCard({
  imageUrl,
  name,
  price,
  onClick,
}: TProductCard): ReactElement {
  return (
    <Root data-testid="ProductCard">
      <ImageWrapper>
        {imageUrl && (
          <img src={imageUrl} alt={name} data-testid="ProductCardImage" />
        )}
      </ImageWrapper>
      <InfoWrapper>
        <TitleWrapper>
          <Title variant="h3" color="secondary">
            {name}
          </Title>
        </TitleWrapper>
        <PriceText price={price} />
        <ActionWrapper>
          <Button type="button" onClick={onClick}>
            Add To Cart
          </Button>
        </ActionWrapper>
      </InfoWrapper>
    </Root>
  );
}

export default ProductCard;
