import React from 'react';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ProductCard from './ProductCard';

describe('ProductCard', () => {
  const mockProps = {
    name: 'Basic Swear Shirt',
    price: 10,
    imageUrl:
      'https://cdn.shopify.com/s/files/1/0049/2856/9434/products/product-05_1024x.jpg?v=1553074064',
  };

  it('renders the ProductCard with default', () => {
    const { getByTestId, getByText } = render(<ProductCard {...mockProps} />);

    expect(getByTestId('ProductCard')).toBeDefined();
    expect(getByTestId('ProductCardImage')).toBeDefined();
    expect(getByText(mockProps.name)).toBeDefined();
    expect(getByText(`${mockProps.price}`)).toBeDefined();
  });

  it('should call onClick when clicked', () => {
    const onClickMock = jest.fn();

    const { getByTestId } = render(
      <ProductCard {...mockProps} onClick={onClickMock} />
    );

    expect(onClickMock).toBeCalledTimes(0);
    userEvent.click(getByTestId('ProductCard'));
    expect(onClickMock).toBeCalledTimes(1);
  });
});
