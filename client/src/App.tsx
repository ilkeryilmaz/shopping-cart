import React, { Suspense, ReactElement } from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import Loader from 'components/Loader';

import DefaultLayout from 'layouts/DefaultLayout';
import { routes } from 'configs/routes';
import Products from 'pages/Products';

function App(): ReactElement {
  return (
    <BrowserRouter>
      <DefaultLayout>
        <Switch>
          {routes.map(({ key, path, exact, component: Component }) => {
            return (
              <Route
                key={key}
                exact={exact}
                path={path}
                component={() => (
                  <Suspense fallback={<Loader />}>
                    <Component />
                  </Suspense>
                )}
              />
            );
          })}
          <Route component={Products} />
        </Switch>
      </DefaultLayout>
    </BrowserRouter>
  );
}

export default App;
