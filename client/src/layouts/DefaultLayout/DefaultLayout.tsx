import React, { ReactElement } from 'react';

import Header from 'components/Header';

import { Wrapper, MainContainer } from './DefaultLayout.style';
import { TDefaultLayout } from './DefaultLayout.types';

function DefaultLayout({ children }: TDefaultLayout): ReactElement {
  return (
    <Wrapper data-testid="DefaultLayout">
      <MainContainer>
        <Header storeName="MyShop." />
        {children}
      </MainContainer>
    </Wrapper>
  );
}

export default DefaultLayout;
