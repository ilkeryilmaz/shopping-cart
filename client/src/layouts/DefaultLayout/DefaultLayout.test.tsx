import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

import AppProvider from 'context/AppContext';
import DefaultLayout from './DefaultLayout';

describe('DefaultLayout', () => {
  test('renders learn react link', () => {
    const { getByTestId } = render(
      <AppProvider>
        <BrowserRouter>
          <DefaultLayout>test</DefaultLayout>
        </BrowserRouter>
      </AppProvider>
    );

    expect(getByTestId('DefaultLayout')).toBeInTheDocument();
  });
});
