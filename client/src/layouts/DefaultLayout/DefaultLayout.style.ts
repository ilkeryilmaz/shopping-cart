import styled from 'styled-components';

export const Wrapper = styled.div(() => ({
  display: 'flex',
  height: '100vh',
  flexDirection: 'row' as const,
}));

export const MainContainer = styled.section(() => ({
  maxWidth: 1300,
  width: '100%',
  marginLeft: 'auto',
  marginRight: 'auto',
}));
