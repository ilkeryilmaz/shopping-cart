import React, { ReactElement, ReactNode } from 'react';
import { Provider as StoreProvider } from 'react-redux';

import { ThemeProvider } from 'themes/themeProvider';
import configureStore from 'configs/store';

type TAppProvider = {
  children: ReactNode;
};

export const { store } = configureStore();

function AppProvider({ children }: TAppProvider): ReactElement {
  return (
    <StoreProvider store={store}>
      <ThemeProvider>{children}</ThemeProvider>
    </StoreProvider>
  );
}

export default AppProvider;
