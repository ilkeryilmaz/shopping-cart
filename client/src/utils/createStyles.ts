import { TBreakpoints } from 'themes/types';
import { TAddDeclarationProps } from 'components/Block/Block.types';

import { getMediaQueries } from './theme';

const isNil = (value: string | Array<string>) =>
  value === null || value === undefined;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TCreateStyles = Record<string, any>;

function createStyles(breakpoints: TBreakpoints): TCreateStyles {
  const styles = {} as TCreateStyles;
  const mediaQueries = getMediaQueries(breakpoints);

  function getValueContent(value: string): string {
    if (!isNil(value)) {
      return value;
    }

    return '';
  }

  return {
    addDeclaration: ({
      property,
      value,
      transform = (arg) => arg,
    }: TAddDeclarationProps) => {
      if (isNil(value)) {
        return;
      }

      if (!Array.isArray(value)) {
        styles[property] = getValueContent(transform(value));
      } else {
        value.forEach((content, index) => {
          if (index === 0) {
            styles[property] = getValueContent(transform(content));
            return;
          }

          const mediaQuery = mediaQueries[index - 1];
          if (!styles[mediaQuery]) {
            styles[mediaQuery] = {} as string;
          }

          styles[mediaQuery][property] = getValueContent(transform(content));
        });
      }
    },
    getStyles: () => styles,
  };
}

export default createStyles;
