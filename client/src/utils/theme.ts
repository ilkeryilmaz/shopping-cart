import { TBreakpoints } from 'themes/types';

export const getMediaQuery = (breakpoint: number): string =>
  `@media screen and (min-width: ${breakpoint}px)`;

export const getMediaQueries = (breakpoints: TBreakpoints): string[] =>
  Object.keys(breakpoints)
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    .map((key) => breakpoints[key])
    .sort((a, b) => a - b)
    .map(getMediaQuery);
