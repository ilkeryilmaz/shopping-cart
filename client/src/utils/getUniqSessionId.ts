import { v4 as uuidv4 } from 'uuid';

import { STORE_STORAGE_KEY } from 'constants/storage';

function getUniqSessionId(): string | null {
  if (!localStorage.getItem(STORE_STORAGE_KEY)) {
    const id = uuidv4();
    localStorage.setItem(STORE_STORAGE_KEY, id);
  }

  return localStorage.getItem(STORE_STORAGE_KEY);
}

export default getUniqSessionId;
