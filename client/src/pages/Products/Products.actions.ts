import { ACTION_TYPES } from './Products.constants';
import {
  TProductsRequestError,
  TProductsRequestFinish,
  TProductsRequestFinishProps,
  TProductsRequestStart,
} from './Products.types';

export type TRequestErrorProps = {
  error?: boolean;
};

export function productsRequestStart(): TProductsRequestStart {
  return {
    type: ACTION_TYPES.PRODUCTS_REQUEST_START,
  };
}

export function productsRequestFinish({
  products,
}: TProductsRequestFinishProps): TProductsRequestFinish {
  return {
    type: ACTION_TYPES.PRODUCTS_REQUEST_FINISH,
    products,
  };
}

export function productsRequestError({
  error,
}: TRequestErrorProps): TProductsRequestError {
  return {
    type: ACTION_TYPES.PRODUCTS_REQUEST_ERROR,
    error,
  };
}
