import { SagaIterator } from 'redux-saga';
import { takeLatest, put, call } from 'redux-saga/effects';

import { API } from 'api';
import { ACTION_TYPES } from './Products.constants';
import * as productsActions from './Products.actions';

export function* productsRequestFlow(): SagaIterator {
  try {
    const { success, data } = yield call(API.GET_PRODUCTS, {});

    if (success) {
      return yield put(
        productsActions.productsRequestFinish({
          products: data,
        })
      );
    }

    return yield put(
      productsActions.productsRequestError({
        error: data.error,
      })
    );
  } catch (e) {
    return yield put(productsActions.productsRequestError({ error: e }));
  }
}

export default function* sagaWatcher(): SagaIterator {
  yield takeLatest(ACTION_TYPES.PRODUCTS_REQUEST_START, productsRequestFlow);
}
