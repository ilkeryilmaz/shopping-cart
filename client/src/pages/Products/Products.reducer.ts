import { ACTION_TYPES } from './Products.constants';
import { TProductsState, TProductsActionTypes } from './Products.types';

export const initialState: TProductsState = {
  loading: false,
  error: false,
  products: [],
};

function productsReducer(
  state = initialState,
  action: TProductsActionTypes
): TProductsState {
  switch (action.type) {
    case ACTION_TYPES.PRODUCTS_REQUEST_START:
      return {
        ...state,
        loading: true,
        error: false,
      };

    case ACTION_TYPES.PRODUCTS_REQUEST_FINISH:
      return {
        ...state,
        products: action.products,
        loading: false,
        error: false,
      };

    case ACTION_TYPES.PRODUCTS_REQUEST_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
      };

    default:
      return state;
  }
}

export default productsReducer;
