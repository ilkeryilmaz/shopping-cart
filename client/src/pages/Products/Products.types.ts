import { ACTION_TYPES } from './Products.constants';

export type TProduct = {
  _id: string;
  name: string;
  price: number;
  imageUrl: string;
};

export type TProductsState = {
  error?: boolean | null;
  loading: boolean;
  products: TProduct[];
};

export type TProductsRequestStart = {
  type: typeof ACTION_TYPES.PRODUCTS_REQUEST_START;
};

export type TProductsRequestFinish = {
  type: typeof ACTION_TYPES.PRODUCTS_REQUEST_FINISH;
  products: TProduct[];
};

export type TProductsRequestFinishProps = {
  products: TProduct[];
};

export type TProductsRequestError = {
  type: typeof ACTION_TYPES.PRODUCTS_REQUEST_ERROR;
  error?: boolean;
};

export type TProductsActionTypes =
  | TProductsRequestStart
  | TProductsRequestFinish
  | TProductsRequestError;
