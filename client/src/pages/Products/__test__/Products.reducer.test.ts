import * as productsActions from '../Products.actions';
import productsReducer, { initialState } from '../Products.reducer';

describe('Products reducer', () => {
  it('should handle ACTION_TYPES/PRODUCTS_REQUEST_START', () => {
    expect(
      productsReducer(initialState, productsActions.productsRequestStart())
    ).toEqual({
      ...initialState,
      loading: true,
    });
  });

  it('should handle ACTION_TYPES/PRODUCTS_REQUEST_FINISH', () => {
    const mockProducts = [
      {
        _id: '5f68a36fd2406a6ff807df1a',
        name: 'Basic Swear Shirt',
        price: 16,
        imageUrl:
          'https://cdn.shopify.com/s/files/1/0049/2856/9434/products/product-05_1024x.jpg?v=1553074064',
      },
    ];

    expect(
      productsReducer(
        initialState,
        productsActions.productsRequestFinish({ products: mockProducts })
      )
    ).toEqual({
      ...initialState,
      products: mockProducts,
    });
  });

  it('should handle ACTION_TYPES/PRODUCTS_REQUEST_ERROR', () => {
    expect(
      productsReducer(
        initialState,
        productsActions.productsRequestError({
          error: true,
        })
      )
    ).toEqual({
      ...initialState,
      error: true,
    });
  });
});
