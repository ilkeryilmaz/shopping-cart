import React from 'react';
import { render } from '@testing-library/react';

import AppProvider from 'context/AppContext';

import Products from '../Products';

describe('Products', () => {
  it('renders the Products with default', () => {
    const { getByTestId } = render(
      <AppProvider>
        <Products />
      </AppProvider>
    );

    expect(getByTestId('Loader')).toBeDefined();
  });
});
