import { ACTION_TYPES } from '../Products.constants';
import * as productsActions from '../Products.actions';

describe('Products actions', () => {
  it('should create an action to products request start', () => {
    const expectedAction = {
      type: ACTION_TYPES.PRODUCTS_REQUEST_START,
    };

    expect(productsActions.productsRequestStart()).toEqual(expectedAction);
  });

  it('should create an action to products request finish', () => {
    const mockProducts = [
      {
        _id: '5f68a36fd2406a6ff807df1a',
        name: 'Basic Swear Shirt',
        price: 16,
        imageUrl:
          'https://cdn.shopify.com/s/files/1/0049/2856/9434/products/product-05_1024x.jpg?v=1553074064',
      },
    ];

    const expectedAction = {
      type: ACTION_TYPES.PRODUCTS_REQUEST_FINISH,
      products: mockProducts,
    };

    expect(
      productsActions.productsRequestFinish({
        products: mockProducts,
      })
    ).toEqual(expectedAction);
  });

  it('should create an action to products request error', () => {
    const expectedAction = {
      type: ACTION_TYPES.PRODUCTS_REQUEST_ERROR,
      error: true,
    };

    expect(
      productsActions.productsRequestError({
        error: true,
      })
    ).toEqual(expectedAction);
  });
});
