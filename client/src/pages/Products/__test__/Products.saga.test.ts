import sagaHelper from 'redux-saga-testing';
import { put, call } from 'redux-saga/effects';

import { API } from 'api';
import { productsRequestFlow } from '../Products.saga';
import * as productsActions from '../Products.actions';

describe('Products saga', () => {
  describe('Scenario 1: When api call finished successfully', () => {
    const mockData = {
      success: true,
      data: [
        {
          _id: '5f68a36fd2406a6ff807df1a',
          name: 'Basic Swear Shirt',
          price: 16,
          imageUrl:
            'https://cdn.shopify.com/s/files/1/0049/2856/9434/products/product-05_1024x.jpg?v=1553074064',
        },
      ],
    };

    const it = sagaHelper(productsRequestFlow() as IterableIterator<unknown>);

    it('should trigger api call', (result) => {
      expect(result).toEqual(call(API.GET_PRODUCTS, {}));
      return mockData;
    });

    it('should trigger finish action', (result) => {
      expect(result).toEqual(
        put(
          productsActions.productsRequestFinish({
            products: mockData.data,
          })
        )
      );
    });
  });
});
