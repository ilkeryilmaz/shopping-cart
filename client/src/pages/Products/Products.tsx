import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import ProductCard from 'components/ProductCard';
import Block from 'components/Block';
import Loader from 'components/Loader';

import { TAppStateProps } from 'types/state';
import * as cartActions from 'pages/Cart/Cart.actions';
import * as productsActions from './Products.actions';
import { TProductsState } from './Products.types';

function Products(): ReactElement {
  const dispatch = useDispatch();
  const { loading, products } = useSelector(
    (state: TAppStateProps): TProductsState => state.products
  );

  useEffect(() => {
    dispatch(productsActions.productsRequestStart());
  }, [dispatch]);

  function handleAddToCart(productId: string) {
    dispatch(
      cartActions.cartRequestAddToCartStart({
        productId,
      })
    );
  }

  if (loading) {
    return <Loader />;
  }

  return (
    <>
      <Block
        display="flex"
        flexDirection="row"
        flexWrap="wrap"
        marginLeft="-16px"
      >
        {products.map(({ _id, name, price, imageUrl }) => (
          <ProductCard
            key={_id}
            name={name}
            price={price}
            imageUrl={imageUrl}
            onClick={() => handleAddToCart(_id)}
          />
        ))}
      </Block>
    </>
  );
}

export default Products;
