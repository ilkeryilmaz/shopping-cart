import { ACTION_TYPES } from './Cart.constants';
import { TCartState, TCartActionTypes } from './Cart.types';

export const initialState: TCartState = {
  loading: false,
  error: false,
  cartItems: [],
};

function cartReducer(
  state = initialState,
  action: TCartActionTypes
): TCartState {
  switch (action.type) {
    case ACTION_TYPES.CART_REQUEST_ADD_TO_CART_START:
      return {
        ...state,
        loading: true,
        error: false,
      };

    case ACTION_TYPES.CART_REQUEST_ADD_TO_CART_FINISH:
      return {
        ...state,
        cartItems: action.cartItems,
        loading: false,
        error: false,
      };

    case ACTION_TYPES.CART_REQUEST_REMOVE_FROM_CART:
      return {
        ...state,
        loading: true,
        error: false,
      };

    case ACTION_TYPES.CART_REQUEST_ADD_TO_CART_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
}

export default cartReducer;
