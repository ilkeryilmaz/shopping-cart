export type TCartItemProps = {
  name: string;
  price: number;
  imageUrl: string;
  total: number;
  onClick: () => void;
};
