import React, { ReactElement } from 'react';

import { DeleteIcon } from 'components/Icons';
import PriceText from 'components/PriceText';
import { Text } from 'components/Typography';

import { Root, Image, Col } from './CartItem.style';
import { TCartItemProps } from './CartItem.types';

function CartItem({
  name,
  price,
  total,
  imageUrl,
  onClick,
}: TCartItemProps): ReactElement {
  return (
    <Root data-testid="CartItem">
      <Col>
        <Image src={imageUrl} alt={name} />
      </Col>
      <Col>
        <Text size="medium">{name}</Text>
      </Col>
      <Col>
        <PriceText price={price} />
      </Col>
      <Col>
        <Text size="medium">{total}</Text>
      </Col>
      <Col>
        <DeleteIcon onClick={onClick} />
      </Col>
    </Root>
  );
}

export default CartItem;
