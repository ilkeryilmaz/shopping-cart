import styled from 'styled-components';

export const Root = styled.div(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  width: '100%',
  borderTop: '1px solid #e9e7e7',
  paddingTop: 16,
  paddingBottom: 16,
  ':last-child': {
    borderBottom: '1px solid #e9e7e7',
  },
}));

export const Image = styled.img(() => ({
  maxWidth: 90,
  height: 'auto',
}));

export const Col = styled.div(() => ({
  flex: 1,
  textAlign: 'center' as const,
}));
