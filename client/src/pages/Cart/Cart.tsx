import React, { ReactElement } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Block from 'components/Block';
import { Text } from 'components/Typography';
import Loader from 'components/Loader';

import { TAppStateProps } from 'types/state';
import PriceText from 'components/PriceText';

import CartItem from './components/CartItem';
import { TCartState } from './Cart.types';
import * as cartActions from './Cart.actions';
import {
  SHIPPING_FREE_LIMIT,
  DEFAULT_SHIPPING_PRICE,
  MAX_SHIPPING_PRICE,
  FREE_SHIPPING_PRICE,
  MAX_SHIPPING_PRICE_PRODUCT_LIMIT,
  DISCOUNT_PRICE_LIMIT,
  DISCOUNT_PRICE,
  DEFAULT_DISCOUNT_PRICE,
} from './Cart.constants';

function Cart(): ReactElement {
  const dispatch = useDispatch();
  const { loading, cartItems } = useSelector(
    (state: TAppStateProps): TCartState => state.cart
  );

  function handleRemoveFromCart(cartId: string) {
    dispatch(cartActions.cartRequestRemoveFromCart(cartId));
  }

  function getCartPrice(): number {
    return cartItems.reduce(
      (prev, current) => prev + current.product.price * current.total,
      0
    );
  }

  function getShippingPrice(): number {
    if (getCartPrice() >= SHIPPING_FREE_LIMIT) {
      return FREE_SHIPPING_PRICE;
    }

    if (
      cartItems.length >= MAX_SHIPPING_PRICE_PRODUCT_LIMIT &&
      getCartPrice() <= SHIPPING_FREE_LIMIT
    ) {
      return MAX_SHIPPING_PRICE;
    }

    return DEFAULT_SHIPPING_PRICE;
  }

  function getDiscountPrice(): number {
    if (getCartPrice() >= DISCOUNT_PRICE_LIMIT) {
      return DISCOUNT_PRICE;
    }

    return DEFAULT_DISCOUNT_PRICE;
  }

  function getTotalPrice(): number {
    return getCartPrice() + getShippingPrice() - getDiscountPrice();
  }

  if (loading) {
    return <Loader />;
  }

  if (cartItems.length === 0) {
    return <div>Your cart is empty...</div>;
  }

  return (
    <>
      <Text size="large" variant="div">
        * Free shipping over <PriceText price={SHIPPING_FREE_LIMIT} /> and above
      </Text>

      <Text size="large" variant="div">
        * <PriceText price={5} /> and above <PriceText price={200} /> discount
      </Text>

      <Block marginTop="24px" marginBottom="24px">
        {cartItems.map(({ product, total }) => (
          <CartItem
            key={product._id}
            name={product.name}
            total={total}
            price={product.price}
            imageUrl={product.imageUrl}
            onClick={() => handleRemoveFromCart(product._id)}
          />
        ))}
      </Block>
      <Block textAlign="right">
        <Text variant="div">
          <b>Price:</b> <PriceText price={getCartPrice()} />
        </Text>
        <Text variant="div">
          <b> Shipping:</b> <PriceText price={getShippingPrice()} />
        </Text>
        {!!getDiscountPrice() && (
          <Text variant="div">
            <b>Discounts:</b> -<PriceText price={getDiscountPrice()} />
          </Text>
        )}
        <Text variant="div">
          <b>Total Price:</b>
          <PriceText price={getTotalPrice()} />
        </Text>
      </Block>
    </>
  );
}

export default Cart;
