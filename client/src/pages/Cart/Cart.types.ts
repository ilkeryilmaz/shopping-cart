import { ACTION_TYPES } from './Cart.constants';
import { TProduct } from '../Products/Products.types';

export type TCartItem = {
  name: string;
  product: TProduct;
  total: number;
};

export type TCartState = {
  error?: boolean | null;
  loading: boolean;
  cartItems: TCartItem[];
};

export type TCartRequestAddToCartStart = {
  type: typeof ACTION_TYPES.CART_REQUEST_ADD_TO_CART_START;
  productId: string;
};

export type TCartRequestAddToCartStartProps = {
  productId: string;
};

export type TCartRequestAddToCartFinish = {
  type: typeof ACTION_TYPES.CART_REQUEST_ADD_TO_CART_FINISH;
  cartItems: TCartItem[];
};

export type TCartRequestAddToCartFinishProps = {
  cartItems: TCartItem[];
};

export type TCartRequestAddToCartError = {
  type: typeof ACTION_TYPES.CART_REQUEST_ADD_TO_CART_ERROR;
  error?: boolean;
};

export type TCartRequestGetCartItems = {
  type: typeof ACTION_TYPES.CART_REQUEST_GET_CART_ITEMS;
};

export type TCartRequestRemoveFromCart = {
  type: typeof ACTION_TYPES.CART_REQUEST_REMOVE_FROM_CART;
  id: string;
};

export type TCartActionTypes =
  | TCartRequestAddToCartStart
  | TCartRequestAddToCartFinish
  | TCartRequestAddToCartError
  | TCartRequestGetCartItems
  | TCartRequestRemoveFromCart;
