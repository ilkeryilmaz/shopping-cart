import { ACTION_TYPES } from './Cart.constants';
import {
  TCartRequestAddToCartStart,
  TCartRequestAddToCartStartProps,
  TCartRequestAddToCartFinish,
  TCartRequestAddToCartFinishProps,
  TCartRequestAddToCartError,
  TCartRequestGetCartItems,
  TCartRequestRemoveFromCart,
} from './Cart.types';

export type TCartRequestAddToCartErrorProps = {
  error?: boolean;
  errorLog?: Error;
};

export function cartRequestAddToCartStart({
  productId,
}: TCartRequestAddToCartStartProps): TCartRequestAddToCartStart {
  return {
    type: ACTION_TYPES.CART_REQUEST_ADD_TO_CART_START,
    productId,
  };
}

export function cartRequestAddToCartFinish({
  cartItems,
}: TCartRequestAddToCartFinishProps): TCartRequestAddToCartFinish {
  return {
    type: ACTION_TYPES.CART_REQUEST_ADD_TO_CART_FINISH,
    cartItems,
  };
}

export function cartRequestGetCartItems(): TCartRequestGetCartItems {
  return {
    type: ACTION_TYPES.CART_REQUEST_GET_CART_ITEMS,
  };
}

export function cartRequestRemoveFromCart(
  id: string
): TCartRequestRemoveFromCart {
  return {
    type: ACTION_TYPES.CART_REQUEST_REMOVE_FROM_CART,
    id,
  };
}

export function cartRequestError({
  error,
}: TCartRequestAddToCartErrorProps): TCartRequestAddToCartError {
  return {
    type: ACTION_TYPES.CART_REQUEST_ADD_TO_CART_ERROR,
    error,
  };
}
