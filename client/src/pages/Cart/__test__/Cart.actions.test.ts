import { ACTION_TYPES } from '../Cart.constants';
import * as cartActions from '../Cart.actions';

describe('Cart actions', () => {
  it('should create an action to add to cart request start', () => {
    const expectedAction = {
      type: ACTION_TYPES.CART_REQUEST_ADD_TO_CART_START,
      productId: '1',
    };

    expect(cartActions.cartRequestAddToCartStart({ productId: '1' })).toEqual(
      expectedAction
    );
  });

  it('should create an action to add to cart request finish', () => {
    const mockCartItems = [
      {
        name: 'name',
        total: 3,
        product: {
          _id: '5f68a36fd2406a6ff807df1a',
          name: 'Basic Swear Shirt',
          price: 16,
          imageUrl:
            'https://cdn.shopify.com/s/files/1/0049/2856/9434/products/product-05_1024x.jpg?v=1553074064',
        },
      },
    ];

    const expectedAction = {
      type: ACTION_TYPES.CART_REQUEST_ADD_TO_CART_FINISH,
      cartItems: mockCartItems,
    };

    expect(
      cartActions.cartRequestAddToCartFinish({
        cartItems: mockCartItems,
      })
    ).toEqual(expectedAction);
  });

  it('should create an action to get cart items', () => {
    const expectedAction = {
      type: ACTION_TYPES.CART_REQUEST_GET_CART_ITEMS,
    };

    expect(cartActions.cartRequestGetCartItems()).toEqual(expectedAction);
  });

  it('should create an action to remove from cart', () => {
    const expectedAction = {
      type: ACTION_TYPES.CART_REQUEST_REMOVE_FROM_CART,
      id: '1',
    };

    expect(cartActions.cartRequestRemoveFromCart('1')).toEqual(expectedAction);
  });

  it('should create an action to products request error', () => {
    const expectedAction = {
      type: ACTION_TYPES.CART_REQUEST_ADD_TO_CART_ERROR,
      error: true,
    };

    expect(
      cartActions.cartRequestError({
        error: true,
      })
    ).toEqual(expectedAction);
  });
});
