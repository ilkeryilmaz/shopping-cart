import * as cartActions from '../Cart.actions';
import cartReducer, { initialState } from '../Cart.reducer';

describe('Products reducer', () => {
  it('should handle ACTION_TYPES/CART_REQUEST_ADD_TO_CART_START', () => {
    expect(
      cartReducer(
        initialState,
        cartActions.cartRequestAddToCartStart({ productId: '1' })
      )
    ).toEqual({
      ...initialState,
      loading: true,
    });
  });

  it('should handle ACTION_TYPES/CART_REQUEST_ADD_TO_CART_FINISH', () => {
    const mockCartItems = [
      {
        name: 'name',
        total: 3,
        product: {
          _id: '5f68a36fd2406a6ff807df1a',
          name: 'Basic Swear Shirt',
          price: 16,
          imageUrl:
            'https://cdn.shopify.com/s/files/1/0049/2856/9434/products/product-05_1024x.jpg?v=1553074064',
        },
      },
    ];

    expect(
      cartReducer(
        initialState,
        cartActions.cartRequestAddToCartFinish({ cartItems: mockCartItems })
      )
    ).toEqual({
      ...initialState,
      cartItems: mockCartItems,
    });
  });

  it('should handle ACTION_TYPES/CART_REQUEST_ADD_TO_CART_ERROR', () => {
    expect(
      cartReducer(
        initialState,
        cartActions.cartRequestError({
          error: true,
        })
      )
    ).toEqual({
      ...initialState,
      error: true,
    });
  });
});
