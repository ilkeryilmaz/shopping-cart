import { SagaIterator } from 'redux-saga';
import { takeLatest, put, call } from 'redux-saga/effects';

import { API } from 'api';
import getUniqSessionId from 'utils/getUniqSessionId';
import { ACTION_TYPES } from './Cart.constants';
import * as cartActions from './Cart.actions';
import {
  TCartRequestAddToCartStart,
  TCartRequestRemoveFromCart,
} from './Cart.types';

export function* cartRequestAddToCartStart({
  productId,
}: TCartRequestAddToCartStart): SagaIterator {
  try {
    const userId = getUniqSessionId();

    const { success, data } = yield call(API.ADD_TO_CART, {
      params: { product_id: productId, user_id: userId },
    });

    if (success) {
      return yield put(
        cartActions.cartRequestAddToCartFinish({
          cartItems: data,
        })
      );
    }

    return yield put(
      cartActions.cartRequestError({
        error: data.error,
      })
    );
  } catch (e) {
    return yield put(cartActions.cartRequestError({ errorLog: e }));
  }
}

export function* cartRequestGetCartItems(): SagaIterator {
  try {
    const userId = getUniqSessionId();

    const { success, data } = yield call(API.GET_CART, {
      params: { user_id: userId },
    });

    if (success) {
      return yield put(
        cartActions.cartRequestAddToCartFinish({
          cartItems: data,
        })
      );
    }

    return yield put(
      cartActions.cartRequestError({
        error: data.error,
      })
    );
  } catch (e) {
    return yield put(cartActions.cartRequestError({ errorLog: e }));
  }
}

export function* cartRequestRemoveFromCart({
  id,
}: TCartRequestRemoveFromCart): SagaIterator {
  try {
    const userId = getUniqSessionId();

    const { success, data } = yield call(API.REMOVE_FROM_CART, {
      params: { user_id: userId, product_id: id },
    });

    if (success) {
      return yield put(
        cartActions.cartRequestAddToCartFinish({
          cartItems: data,
        })
      );
    }

    return yield put(
      cartActions.cartRequestError({
        error: data.error,
      })
    );
  } catch (e) {
    return yield put(cartActions.cartRequestError({ errorLog: e }));
  }
}

export default function* sagaWatcher(): SagaIterator {
  yield takeLatest(
    ACTION_TYPES.CART_REQUEST_ADD_TO_CART_START,
    cartRequestAddToCartStart
  );
  yield takeLatest(
    ACTION_TYPES.CART_REQUEST_GET_CART_ITEMS,
    cartRequestGetCartItems
  );
  yield takeLatest(
    ACTION_TYPES.CART_REQUEST_REMOVE_FROM_CART,
    cartRequestRemoveFromCart
  );
}
